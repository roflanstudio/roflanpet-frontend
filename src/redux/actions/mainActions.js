import * as types from "./constants.js";

export function getCount() {
    return {
        type: types.GET_COUNT,
        payload: {
            client: "python",
            request: {
                url: `leaflets/main/`,
                method: "get"
            },
        },
    };
}
export function postLeaflet(data) {
    return {
        type: types.POST_LEAFLET,
        payload: {
            client: "python",
            request: {
                url: `leaflets/create/`,
                method: "post",
                data
            },
        },
    };
}
export function postPetPhoto(data) {
    return {
        type: types.POST_PHOTO,
        payload: {
            client: "python",
            request: {
                url: `pets/add-photo/`,
                method: "post",
                data
            },
        },
    };
}
