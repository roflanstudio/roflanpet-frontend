import * as types from "./constants.js";

export function postLogin(data) {
    return {
        type: types.LOGIN,
        payload: {
            client: "python",
            request: {
                url: `auth/`,
                method: "post",
                data,
            },
        },
    };
}

export function register(data) {
    return {
        type: types.REGISTER,
        payload: {
            client: "python",
            request: {
                url: `auth/register/`,
                method: "post",
                data,
            },
        },
    };
}
