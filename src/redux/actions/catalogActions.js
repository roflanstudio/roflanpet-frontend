import * as types from './constants.js'

export function getCity() {
    return {
        type: types.GET_CITY,
        payload: {
            client: 'java',
            request: {
                url: `city/`,
                method: 'get',
            },
        },
    }
}

export function getSpecies() {
    return {
        type: types.GET_SPECIES,
        payload: {
            client: 'java',
            request: {
                url: `species/`,
                method: 'get',
            },
        },
    }
}

export function getBreed(speciesId) {
    return {
        type: types.GET_BREED,
        payload: {
            client: 'java',
            request: {
                url: `bread/${speciesId}/`,
                method: 'get',
            },
        },
    }
}

export function getAllPosts(
    status,
    cityId,
    petGender,
    petSize,
    petSpeciesId,
    petBreadId
) {
    return {
        type: types.GET_ALL_POSTS,
        payload: {
            client: 'java',
            request: {
                url: `leaflet?pageSize=100&status=${status}&cityId=${cityId}&petGender=${petGender}&petSize=${petSize}&petSpeciesId=${petSpeciesId}&petBreadId=${petBreadId}`,
                method: 'get',
            },
        },
    }
}

export function getDetails(id) {
    return {
        type: types.GET_DETAILS,
        payload: {
            client: 'java',
            request: {
                url: `leaflet/${id}`,
                method: 'get',
            },
        },
    }
}