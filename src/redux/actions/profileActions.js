import * as types from './constants.js'

export function getProfile() {
    return {
        type: types.GET_PROFILE,
        payload: {
            client: 'python',
            request: {
                url: `auth/me/`,
                method: 'get',
            },
        },
    }
}

export function getPets() {
    return {
        type: types.GET_PETS,
        payload: {
            client: 'python',
            request: {
                url: `pets/my/`,
                method: 'get',
            },
        },
    }
}

export function getPosts() {
    return {
        type: types.GET_POSTS,
        payload: {
            client: 'python',
            request: {
                url: `leaflets/my/`,
                method: 'get',
            },
        },
    }
}

export function getHits(id) {
    return {
        type: types.GET_HITS,
        payload: {
            client: 'python',
            request: {
                url: `leaflets/hit/${id}`,
                method: 'get',
            },
        },
    }
}

export function getQrHits(id) {
    return {
        type: types.GET_QR_HITS,
        payload: {
            client: 'python',
            request: {
                url: `leaflets/hit_qr/${id}`,
                method: 'get',
            },
        },
    }
}
