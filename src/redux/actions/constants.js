//////////////////////////////auth actions//////////////////////////////

export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'

export const REGISTER = 'REGISTER'
export const REGISTER_FAIL = 'REGISTER_FAIL'

export const VERIFY_EMAIL = 'VERIFY_EMAIL'

///////////////////////////////////MAIN////////////////////////

export const GET_COUNT = 'GET_COUNT'
export const GET_COUNT_SUCCESS = 'GET_COUNT_SUCCESS'

export const POST_LEAFLET = 'POST_LEAFLET'

export const POST_PHOTO = 'POST_PHOTO'

//////////////////////PROFILE////////////////////

export const GET_PROFILE = 'GET_PROFILE'
export const GET_PROFILE_SUCCESS = 'GET_PROFILE_SUCCESS'

export const GET_PETS = 'GET_PETS'
export const GET_PETS_SUCCESS = 'GET_PETS_SUCCESS'

export const GET_POSTS = 'GET_POSTS'
export const GET_POSTS_SUCCESS = 'GET_POSTS_SUCCESS'

////////////////////////CATALOG/////////////////

export const GET_CITY = 'GET_CITY'
export const GET_CITY_SUCCESS = 'GET_CITY_SUCCESS'

export const GET_BREED = 'GET_BREED'
export const GET_BREED_SUCCESS = 'GET_BREED_SUCCESS'

export const GET_SPECIES = 'GET_SPECIES'
export const GET_SPECIES_SUCCESS = 'GET_SPECIES_SUCCESS'

export const GET_ALL_POSTS = 'GET_ALL_POSTS'
export const GET_ALL_POSTS_SUCCESS = 'GET_ALL_POSTS_SUCCESS'

export const GET_DETAILS = 'GET_DETAILS'
export const GET_DETAILS_SUCCESS = 'GET_DETAILS_SUCCESS'

export const GET_HITS = 'GET_HITS'
export const GET_HITS_SUCCESS = 'GET_HITS_SUCCESS'

export const GET_QR_HITS = 'GET_QR_HITS'
export const GET_QR_HITS_SUCCESS = 'GET_QR_HITS_SUCCESS'
