import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { reducer as formReducer } from "redux-form";
import AuthReducer from "./authReducer";
import MainReducer from "./mainReducer";
import ProfileReducer from "./profileReducer";
import CatalogReducer from "./catalogReducer";

const rootReducer = (history) =>
    combineReducers({
        router: connectRouter(history),
        form: formReducer,
        auth: AuthReducer,
        main: MainReducer,
        profile: ProfileReducer,
        catalog: CatalogReducer,
    });

export default rootReducer;
