import * as types from '../actions/constants'

const INITIAL_STATE = {
    city: [],
    species: [],
    breed: [],
    allPosts: [],
    details: {},
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.GET_CITY_SUCCESS:
            return {
                ...state,
                city: action.payload.data,
            }
        case types.GET_SPECIES_SUCCESS:
            return {
                ...state,
                species: action.payload.data,
            }
        case types.GET_BREED_SUCCESS:
            return {
                ...state,
                breed: action.payload.data,
            }
        case types.GET_ALL_POSTS_SUCCESS:
            return {
                ...state,
                allPosts: action.payload.data,
            }
        case types.GET_DETAILS_SUCCESS:
            return {
                ...state,
                details: action.payload.data,
            }
        default:
            return state
    }
}
