import * as types from "../actions/constants";

const INITIAL_STATE = {
    email: "",
    token: "",
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.LOGIN_SUCCESS:
            localStorage.setItem("token", action.payload.data.token);
            localStorage.setItem("email", action.payload.data.email);
            return {
                ...state,
                token: action.payload.data.token,
                email: action.payload.data.email,
            };
        default:
            return state;
    }
}
