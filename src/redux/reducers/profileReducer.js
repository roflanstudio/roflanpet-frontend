import * as types from '../actions/constants'

const INITIAL_STATE = {
    profile: '',
    pets: [],
    posts: [],
    hits: {},
    hits_qr: {},
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.GET_PROFILE_SUCCESS:
            return {
                ...state,
                profile: action.payload.data,
            }
        case types.GET_PETS_SUCCESS:
            return {
                ...state,
                pets: action.payload.data,
            }
        case types.GET_POSTS_SUCCESS:
            return {
                ...state,
                posts: action.payload.data,
            }
        case types.GET_HITS_SUCCESS:
            return {
                ...state,
                hits: action.payload.data,
            }
        case types.GET_QR_HITS_SUCCESS:
            return {
                ...state,
                hits_qr: action.payload.data,
            }
        default:
            return state
    }
}
