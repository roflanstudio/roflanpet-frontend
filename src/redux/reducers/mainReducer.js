import * as types from "../actions/constants";

const INITIAL_STATE = {
    count: "",
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.GET_COUNT_SUCCESS:
            return {
                ...state,
                count: action.payload.data,
            };
        default:
            return state;
    }
}
