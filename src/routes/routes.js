import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import App from '../containers/App'
import AuthContainer from './../containers/AuthContainer'
import MainContainer from './../containers/MainContainer'

const routes = (
    <App>
        <Switch>
            <Route path='/' exact render={() => <Redirect to='/main/home' />} />
            <Route path='/main' component={MainContainer} />
            <Route path='/auth' component={AuthContainer} />
            <Route render={() => <div>404</div>} />
        </Switch>
        <ToastContainer />
    </App>
)

export default routes
