import React from 'react'
import { Switch, Route, Redirect, useRouteMatch } from 'react-router-dom'
import HomePage from './../components/Main/HomePage/HomePage'
import Header from './../components/HelperComponents/Header/Header'
import Profile from '../components/Profile/Profile'
import Catalog from './../components/Catalog/Catalog'
import Details from './../components/Details/Details'

const MainContainer = () => {
    const { path } = useRouteMatch()

    return (
        <>
            <Header />
            <Switch>
                <Route path={`${path}/home`} exact component={HomePage} />
                <Route path={`${path}/profile`} exact component={Profile} />
                <Route path={`${path}/catalog`} exact component={Catalog} />
                <Route path={`${path}/catalog/:id`} exact component={Details} />
            </Switch>
        </>
    )
}

export default MainContainer
