import React from "react";
import { Switch, Route, Redirect, useRouteMatch } from "react-router-dom";

import Login from "../components/Auth/Login/Login";
import Register from "../components/Auth/Register/Register";

const AuthContainer = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/login`} exact component={Login} />
            <Route path={`${path}/register`} exact component={Register} />
        </Switch>
    );
};

export default AuthContainer;
