import React from 'react'
import './Profile.scss'
import noImage from '../../assets/images/no-image.png'
import { Link } from 'react-router-dom'

const PostCard = ({
    id,
    pic,
    name,
    breed,
    type,
    gender,
    size,
    color,
    city,
    status,
}) => {
    console.log(pic)
    return (
        <Link to={`/main/catalog/${id}`} className='pet-card'>
            <img src={pic ? pic : noImage} />
            <div className='name-type'>
                {name && <span>Имя: {name}</span>}
                {type && <span>{type}</span>}
            </div>
            {city && <p className='city'>Город: {city}</p>}
            {breed && <p className='breed'>Породa: {breed}</p>}
            {gender && <p className='gender'>Пол: {gender}</p>}
            {size && <p className='size'>Размер: {size}</p>}
            {color && <p className='color'>Цвет: {color}</p>}
            {status && <p className='status'>{status}</p>}
        </Link>
    )
}

export default PostCard
