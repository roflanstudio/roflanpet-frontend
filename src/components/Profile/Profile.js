import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import {
    getProfile,
    getPets,
    getPosts,
} from './../../redux/actions/profileActions'
import './Profile.scss'
import cat from '../../assets/images/auth-bg.png'
import PetCard from './PetCard'
import PostCard from './PostCard'
import FadeLoader from 'react-spinners/FadeLoader'
import { useState } from 'react'

const Profile = ({
    getProfile,
    getPets,
    getPosts,
    profileInfo: { first_name, last_name, email, phone_number },
    pets,
    posts,
}) => {
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Promise.all([getProfile(), getPets(), getPosts()]).then(values => {
            setLoading(false)
        })
    }, [])
    return (
        <>
            {loading && (
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        paddingTop: '400px',
                    }}>
                    <FadeLoader />
                </div>
            )}
            {!loading && (
                <div className='profile'>
                    <p className='cabinet'>Личный Кабинет</p>
                    <div className='info'>
                        <img src={cat} alt='profile' />
                        <div className='data'>
                            {first_name && last_name && (
                                <p className='name'>{`${first_name} ${last_name}`}</p>
                            )}
                            {phone_number && (
                                <a
                                    href={`tel:${phone_number}`}
                                    className='phone'>
                                    <svg
                                        width='25'
                                        height='25'
                                        viewBox='0 0 25 25'
                                        fill='none'
                                        xmlns='http://www.w3.org/2000/svg'>
                                        <path
                                            d='M10.2857 14.5774C11.7555 16.0684 13.5339 17.2195 15.496 17.95L18.1504 15.8377C18.2291 15.7835 18.3224 15.7545 18.4179 15.7545C18.5135 15.7545 18.6068 15.7835 18.6855 15.8377L23.6141 19.0131C23.8012 19.1256 23.9596 19.2801 24.0767 19.4643C24.1939 19.6485 24.2666 19.8575 24.2891 20.0746C24.3117 20.2918 24.2834 20.5112 24.2066 20.7156C24.1297 20.9199 24.0064 21.1036 23.8464 21.2521L21.537 23.5334C21.2063 23.8601 20.7998 24.1 20.3538 24.2314C19.9079 24.3629 19.4363 24.3819 18.9812 24.2867C14.4414 23.3491 10.257 21.1532 6.90612 17.95C3.62964 14.7149 1.36234 10.5985 0.379248 6.10021C0.282131 5.65216 0.302401 5.18661 0.438093 4.7487C0.573785 4.31078 0.820298 3.91535 1.15374 3.60071L3.54763 1.29131C3.69577 1.13908 3.87643 1.02233 4.07608 0.949811C4.27573 0.877292 4.4892 0.85088 4.70051 0.872553C4.91181 0.894225 5.11548 0.96342 5.29625 1.07496C5.47703 1.18649 5.63022 1.33748 5.74438 1.51662L9.02541 6.38184C9.082 6.45824 9.11255 6.5508 9.11255 6.64587C9.11255 6.74095 9.082 6.83351 9.02541 6.90991L6.86387 9.50798C7.61413 11.4308 8.78302 13.1625 10.2857 14.5774Z'
                                            fill='black'
                                        />
                                    </svg>
                                    <span>{phone_number}</span>
                                </a>
                            )}
                            {email && (
                                <a href={`mailto:${email}`} className='email'>
                                    <svg
                                        width='31'
                                        height='24'
                                        viewBox='0 0 31 24'
                                        fill='none'
                                        xmlns='http://www.w3.org/2000/svg'>
                                        <path
                                            d='M28.9344 0.236397H1.07164C0.478891 0.236397 0 0.715287 0 1.30804V22.7409C0 23.3337 0.478891 23.8126 1.07164 23.8126H28.9344C29.5271 23.8126 30.006 23.3337 30.006 22.7409V1.30804C30.006 0.715287 29.5271 0.236397 28.9344 0.236397ZM27.5948 3.94696V21.4014H2.4112V3.94696L1.48691 3.22695L2.80302 1.53576L4.23634 2.65094H25.773L27.2064 1.53576L28.5225 3.22695L27.5948 3.94696ZM25.773 2.64759L15.003 11.0198L4.23299 2.64759L2.79967 1.53242L1.48356 3.2236L2.40785 3.94361L13.8476 12.8383C14.1766 13.0938 14.5814 13.2326 14.998 13.2326C15.4146 13.2326 15.8193 13.0938 16.1483 12.8383L27.5948 3.94696L28.5191 3.22695L27.203 1.53576L25.773 2.64759Z'
                                            fill='black'
                                        />
                                    </svg>
                                    <span>{email}</span>
                                </a>
                            )}
                        </div>
                    </div>
                    {pets && pets.length > 0 && (
                        <>
                            <p className='my-pets'>Мои Питомцы</p>
                            <div className='pets-list'>
                                {pets.map(el => (
                                    <PetCard
                                        key={el.id}
                                        pic={
                                            el.images.length > 0
                                                ? el.images[0] &&
                                                  el.images[0].pic
                                                : null
                                        }
                                        color={el.color}
                                        gender={el.gender}
                                        breed={el.breed}
                                        name={el.name && el.name.substring(0, 7)}
                                        type={el.species}
                                        size={el.size}
                                    />
                                ))}
                            </div>{' '}
                        </>
                    )}
                    {posts && posts.length > 0 && (
                        <>
                            <p className='my-posts'>Мои Объявления</p>
                            <div className='pets-list'>
                                {posts.length > 0 &&
                                    posts.map(el => (
                                        <PostCard
                                            id={el.id}
                                            key={el.id}
                                            pic={
                                                el.pet.images.length > 0
                                                    ? el.pet.images[0] &&
                                                      el.pet.images[0].pic
                                                    : null
                                            }
                                            color={el.pet.color}
                                            gender={el.pet.gender}
                                            breed={el.pet.breed}
                                            name={el.pet && el.pet.name && el.pet.name.substring(0, 7)}
                                            type={el.pet.species}
                                            size={el.pet.size}
                                            status={el.status}
                                            city={el.city}
                                        />
                                    ))}
                            </div>
                        </>
                    )}
                </div>
            )}
        </>
    )
}

const mapStateToProps = state => {
    return {
        profileInfo: state.profile.profile,
        pets: state.profile.pets,
        posts: state.profile.posts,
    }
}

const mapDispatchToProps = {
    getProfile,
    getPets,
    getPosts,
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
