import React from 'react'
import './Profile.scss'
import noImage from '../../assets/images/no-image.png'

const PetCard = ({ pic, name, breed, type, gender, size, color }) => {
    return (
        <div className='pet-card'>
            <img src={pic ? pic : noImage} />
            <div className='name-type'>
                {name && <span>Имя: {name}</span>}
                {type && <span>{type}</span>}
            </div>
            {breed && <p className='breed'>Породa: {breed}</p>}
            {gender && <p className='gender'>Пол: {gender}</p>}
            {size && <p className='size'>Размер: {size}</p>}
            {color && <p className='color'>Цвет: {color}</p>}
        </div>
    )
}

export default PetCard
