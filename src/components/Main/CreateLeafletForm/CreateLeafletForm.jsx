import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm, SubmissionError } from 'redux-form'
import RenderField from '../../HelperComponents/RenderField/RenderField'
import DialogComponent from '../../HelperComponents/DialogComponent/DialogComponent'
import FadeLoader from 'react-spinners/FadeLoader'
import { postLeaflet, postPetPhoto } from './../../../redux/actions/mainActions'
import {
    getCity,
    getSpecies,
    getBreed,
} from './../../../redux/actions/catalogActions'
import SelectComponent from '../../HelperComponents/SelectComponent/SelectComponent'

import './CreateLeafletForm.scss'

let CreateLeafletForm = ({
    isOpen,
    close,
    type,
    postLeaflet,
    handleSubmit,
    error,
    getCity,
    getSpecies,
    getBreed,
    cities,
    speciesOptions,
    breedOptions,
    postPetPhoto,
}) => {
    const genderOptions = [
        { label: 'Мужской пол', value: 'm' },
        { label: 'Женский пол', value: 'f' },
    ]
    const sizeOptions = [
        { label: 'Очень маленький', value: 't' },
        { label: 'Маленький', value: 's' },
        { label: 'Средний', value: 'm' },
        { label: 'Большой', value: 'l' },
        { label: 'Огромный', value: 'h' },
    ]

    const [gender, setGender] = useState(null)
    const [size, setSize] = useState(null)
    const [loading, setLoading] = useState(true)
    const [city, setCity] = useState(null)
    const [species, setspecies] = useState(null)
    const [breed, setBreed] = useState(null)
    const [photos, setPhotos] = useState([])

    useEffect(() => {
        Promise.all([getCity(), getSpecies()]).then(() => setLoading(false))
    }, [])
    useEffect(() => {
        species && getBreed(species.value)
    }, [species])

    const submitForm = form_values => {
        const email = form_values.email
        delete form_values.email
        const data = {
            pet: {
                ...form_values,
                gender: gender && gender.value,
                size: size && size.value,
                species: species && species.value,
                bread: species.value ? gender && gender.value : null,
            },
            status: type,
            city: city && city.value,
            email: localStorage.getItem('email')
                ? localStorage.getItem('email')
                : email,
        }
        return postLeaflet(data).then(res => {
            if (
                res.payload &&
                res.payload.status &&
                res.payload.status === 201
            ) {
                const pet = res.payload.data.pet.id
                setLoading(true)
                Promise.all(
                    photos.map(obj => {
                        const fd = new FormData()
                        fd.append('pet', pet)
                        fd.append('pic', obj)
                        return postPetPhoto(fd)
                    })
                ).then(() => close())
            } else {
                throw new SubmissionError({
                    _error: 'kek',
                })
            }
        })
    }

    return (
        <DialogComponent isOpen={isOpen} close={close}>
            {loading ? (
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <FadeLoader color='#ffffff' />
                </div>
            ) : (
                <div style={{ display: 'flex' }}>
                    <form
                        onSubmit={handleSubmit(submitForm)}
                        style={{ width: '50%' }}>
                        {!localStorage.getItem('email') && (
                            <div className='register-input'>
                                <Field
                                    name='email'
                                    type='email'
                                    placeholder='Email'
                                    component={RenderField}
                                    color={'#B87C84'}
                                />
                            </div>
                        )}
                        <div className='register-input'>
                            <Field
                                name='name'
                                type='text'
                                placeholder='Кличка'
                                component={RenderField}
                                color={'#B87C84'}
                            />
                        </div>
                        <div className='register-input'>
                            <SelectComponent
                                value={species}
                                options={speciesOptions.map(obj => ({
                                    label: obj.name,
                                    value: obj.id,
                                }))}
                                change={setspecies}
                                isClearable
                                isSearchable={false}
                                placeholder='Вид'
                            />
                        </div>
                        {species && (
                            <div className='register-input'>
                                <SelectComponent
                                    value={breed}
                                    options={breedOptions.map(obj => ({
                                        label: obj.name,
                                        value: obj.id,
                                    }))}
                                    change={setBreed}
                                    isClearable
                                    isSearchable={false}
                                    placeholder='Вид'
                                />
                            </div>
                        )}
                        <div className='register-input'>
                            <SelectComponent
                                value={gender}
                                options={genderOptions}
                                change={setGender}
                                isClearable
                                isSearchable={false}
                                placeholder='Пол'
                            />
                        </div>
                        <div className='register-input'>
                            <SelectComponent
                                value={size}
                                options={sizeOptions}
                                change={setSize}
                                isClearable
                                isSearchable={false}
                                placeholder='Размер'
                            />
                        </div>
                        <div className='register-input'>
                            <SelectComponent
                                value={city}
                                options={cities.map(obj => ({
                                    label: obj.name,
                                    value: obj.id,
                                }))}
                                change={setCity}
                                isClearable
                                isSearchable={false}
                                placeholder='Город'
                            />
                        </div>
                        <div className='register-input'>
                            <Field
                                name='color'
                                type='text'
                                placeholder='Цвет'
                                component={RenderField}
                                color={'#B87C84'}
                            />
                        </div>
                        <div className='register-input'>
                            <Field
                                name='extras'
                                type='text'
                                placeholder='Особые приметы'
                                component={RenderField}
                                color={'#B87C84'}
                                multiline
                            />
                        </div>

                        {error && <span className='main-error'>{error}</span>}
                        <button type='submit' className="form-btn">Создать объявления</button>
                    </form>
                    <div style={{ display: 'flex' }}>
                        <input
                            style={{ display: 'none' }}
                            accept={'.png, .jpg'}
                            type='file'
                            multiple
                            id='photos'
                            style={{ display: 'none' }}
                            onChange={e =>
                                setPhotos(photos => [
                                    ...photos,
                                    ...e.target.files,
                                ])
                            }
                        />
                        <label className='upload' htmlFor='photos'>
                            Загрузить фото{' '}
                            {photos.length > 0 &&
                                `(Загруженно ${photos.length})`}
                        </label>
                    </div>
                </div>
            )}
        </DialogComponent>
    )
}

const validate = values => {
    const errors = {}
    if (!values.email && !localStorage.getItem('email')) {
        errors.email = 'Required field'
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    return errors
}

CreateLeafletForm = reduxForm({
    form: 'LeafletForm',
    validate,
})(CreateLeafletForm)

const mapStateToProps = state => {
    return {
        cities: state.catalog.city,
        speciesOptions: state.catalog.species,
        breedOptions: state.catalog.breed,
    }
}

const mapDispatchToProps = {
    postLeaflet,
    getCity,
    getSpecies,
    getBreed,
    postPetPhoto,
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateLeafletForm)
