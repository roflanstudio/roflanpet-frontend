import React, { Component } from 'react'
import './HomePage.scss'
import Badge from './Badge'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getCount } from './../../../redux/actions/mainActions'
import dog from '../../../assets/images/dog.png'
import paws from '../../../assets/images/paws.png'
import CreateLeafletForm from '../CreateLeafletForm/CreateLeafletForm'
import { toast } from 'react-toastify'

class HomePage extends Component {
    state = {
        openModal: false,
        modalType: null,
    }

    componentDidMount() {
        const { getCount } = this.props
        getCount()
    }
    amountToString(amount) {
        switch (amount.toString().length) {
            case 0:
                return '00000'
            case 1:
                return '0000' + amount
            case 2:
                return '000' + amount
            case 3:
                return '00' + amount
            case 4:
                return '0' + amount
            case 5:
                return amount.toString()
            default:
                return '00000'
        }
    }

    render() {
        const { count } = this.props

        return (
            <>
                <div className='home-page'>
                    <div className='home-first-part'>
                        <p>
                            <span>Здесь</span> вы можете найти своего питомца
                            или помочь в поиске питомца другому владельцу
                        </p>
                        <div className='counter'>
                            <p className='already-found'>Мы уже нашли</p>
                            <div>
                                {this.amountToString(count && count.count)
                                    .split('')
                                    .map(el => (
                                        <div className='amount-saved'>{el}</div>
                                    ))}
                            </div>
                            <p className='your-pets'>Ваших домашних любимцев</p>
                        </div>
                        <div className='home-btns'>
                            <div
                                onClick={() =>
                                    this.setState({
                                        openModal: true,
                                        modalType: 's',
                                    })
                                }>
                                Я потерял питомца
                            </div>
                            <div
                                onClick={() =>
                                    this.setState({
                                        openModal: true,
                                        modalType: 'f',
                                    })
                                }>
                                Я нашел питомца
                            </div>
                        </div>
                    </div>
                    <Link to='/main/catalog/' className='link-catalog'>
                        <p>Каталог</p>
                        <svg
                            className='filll'
                            width='84'
                            height='74'
                            viewBox='0 0 84 74'
                            fill='none'
                            xmlns='http://www.w3.org/2000/svg'>
                            <path
                                d='M41.9999 31.8214C29.1489 31.8214 10.9285 51.6877 10.9285 64.228C10.9285 69.8758 15.2671 73.25 22.5382 73.25C30.442 73.25 35.661 69.1913 41.9999 69.1913C48.3938 69.1913 53.6274 73.25 61.4616 73.25C68.7327 73.25 73.0713 69.8758 73.0713 64.228C73.0713 51.6877 54.8508 31.8214 41.9999 31.8214ZM18.1655 29.7808C16.4825 24.1733 11.2974 20.5419 6.58493 21.6682C1.87243 22.7945 -0.582541 28.2531 1.1005 33.8605C2.78353 39.4679 7.96858 43.0994 12.6811 41.9731C17.3936 40.8467 19.8485 35.3882 18.1655 29.7808ZM31.8758 26.4179C36.8828 25.1006 39.388 18.3361 37.4719 11.3094C35.5558 4.28277 29.9435 -0.343961 24.9365 0.973338C19.9295 2.29064 17.4243 9.05515 19.3404 16.0818C21.2565 23.1085 26.8704 27.7368 31.8758 26.4179ZM77.4132 21.6698C72.7007 20.5435 67.5173 24.175 65.8326 29.7824C64.1496 35.3898 66.6046 40.8483 71.3171 41.9747C76.0296 43.101 81.213 39.4695 82.8977 33.8621C84.5807 28.2547 82.1257 22.7962 77.4132 21.6698ZM52.124 26.4179C57.131 27.7352 62.7433 23.1085 64.6594 16.0818C66.5755 9.05515 64.0703 2.29225 59.0633 0.973338C54.0563 -0.34558 48.444 4.28277 46.5279 11.3094C44.6118 18.3361 47.117 25.1006 52.124 26.4179Z'
                                fill='white'
                            />
                        </svg>
                        <p>Питомцев</p>
                    </Link>
                    <div className='home-second-part' id='badge'>
                        <Badge />
                        <div className='afraid'>
                            Боитесь потерять домашнее животное?
                        </div>
                        <div className='garant'>
                            Жетон Petify гарантирует вам спокойствие за вашего
                            питомца
                        </div>
                        <div className='own-pet'>
                            Зарегистрируйте своего питомца на нашем сайте и
                            получите фирменый жетон который облегчит поиски
                            вашего любимца
                        </div>
                        <div
                            className='reg-pet'
                            style={{ cursor: 'pointer' }}
                            onClick={() => toast('Мы скоро с вами свяжемся')}>
                            Зарегистрировать питомца
                            <svg
                                width='64'
                                height='57'
                                viewBox='0 0 64 57'
                                fill='none'
                                xmlns='http://www.w3.org/2000/svg'>
                                <path
                                    d='M32.1018 24.7187C22.2221 24.7187 8.21431 39.9918 8.21431 49.6326C8.21431 53.9747 11.5499 56.5687 17.1398 56.5687C23.2162 56.5687 27.2285 53.4484 32.1018 53.4484C37.0174 53.4484 41.041 56.5687 47.0639 56.5687C52.6538 56.5687 55.9893 53.9747 55.9893 49.6326C55.9893 39.9918 41.9815 24.7187 32.1018 24.7187ZM13.7781 23.1498C12.4842 18.8389 8.49798 16.047 4.87504 16.913C1.2521 17.7789 -0.635263 21.9754 0.658643 26.2863C1.95255 30.5973 5.93878 33.3891 9.56172 32.5232C13.1847 31.6573 15.072 27.4608 13.7781 23.1498V23.1498ZM24.3185 20.5645C28.1678 19.5518 30.0938 14.3513 28.6207 8.94922C27.1477 3.54716 22.833 -0.00983981 18.9836 1.00289C15.1342 2.01562 13.2083 7.21613 14.6814 12.6182C16.1544 18.0202 20.4703 21.5785 24.3185 20.5645ZM59.3274 16.9142C55.7044 16.0483 51.7194 18.8401 50.4243 23.1511C49.1304 27.462 51.0177 31.6585 54.6407 32.5244C58.2636 33.3904 62.2486 30.5985 63.5437 26.2876C64.8377 21.9766 62.9503 17.7801 59.3274 16.9142V16.9142ZM39.8852 20.5645C43.7345 21.5772 48.0492 18.0202 49.5223 12.6182C50.9953 7.21613 49.0694 2.01687 45.22 1.00289C41.3707 -0.0110839 37.056 3.54716 35.5829 8.94922C34.1099 14.3513 36.0358 19.5518 39.8852 20.5645V20.5645Z'
                                    fill='white'
                                />
                            </svg>
                        </div>
                        <img
                            src={dog}
                            alt='dog'
                            style={{
                                position: 'absolute',
                                bottom: '34px',
                                right: '34px',
                            }}
                        />
                        <img
                            src={paws}
                            alt='paws'
                            style={{
                                position: 'absolute',
                                bottom: '24px',
                                left: '12px',
                            }}
                        />
                    </div>
                </div>
                {this.state.openModal && (
                    <CreateLeafletForm
                        isOpen={this.state.openModal}
                        type={this.state.modalType}
                        close={() => this.setState({ openModal: false })}
                    />
                )}
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        count: state.main.count,
    }
}

const mapDispatchToProps = {
    getCount,
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
