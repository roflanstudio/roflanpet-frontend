import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm, SubmissionError } from "redux-form";
import RenderField from "../../HelperComponents/RenderField/RenderField";
import AuthBtn from "./../../HelperComponents/AuthBtn/AuthBtn";
import { postLogin } from "./../../../redux/actions/authActions";

class Login extends Component {
    submitForm = (data) => {
        const { postLogin, history } = this.props;
        console.log(data);
        return postLogin(data).then((res) => {
            if (
                res.payload &&
                res.payload.status &&
                res.payload.status === 200
            ) {
                history.push("/main/home");
            } else {
                throw new SubmissionError({
                    password:
                        res.error.response.data.password &&
                        res.error.response.data.password[0],
                    email:
                        res.error.response.data.email &&
                        res.error.response.data.email[0],
                    _error: res.error.response.data.non_field_errors[0],
                });
            }
        });
    };

    render() {
        const { handleSubmit, error } = this.props;
        return (
            <div className="auth-background">
                <div className="register">
                    <h1 className="register-title">Sign In</h1>
                    <form onSubmit={handleSubmit(this.submitForm)}>
                        <div className="register-input">
                            <Field
                                name="username"
                                type="email"
                                placeholder="Email"
                                component={RenderField}
                                color={"#B87C84"}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="password"
                                type="password"
                                placeholder="Password"
                                component={RenderField}
                            />
                        </div>
                        {error && <span className="main-error">{error}</span>}
                        <div className="register-login">
                            <div className="register-login-text">
                                Don’t have an account?
                            </div>
                            <Link
                                className="register-login-link"
                                to="/auth/register"
                            >
                                Sign up
                            </Link>
                        </div>
                        <AuthBtn />
                    </form>
                </div>
            </div>
        );
    }
}

const validate = (values) => {
    const errors = {};
    if (!values.username) {
        errors.username = "Required field";
    }
    if (!values.email) {
        errors.email = "Required field";
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
    ) {
        errors.email = "Invalid email address";
    }
    if (!values.password) {
        errors.password = "Required field";
    } else if (values.password.length < 4) {
        errors.password = "Password must contain 4 or more symbols";
    }
    if (!values.password_confirm) {
        errors.password_confirm = "Required field";
    } else if (values.password !== values.password_confirm) {
        errors.password_confirm = "Passwords are not the same";
    }
    return errors;
};

Login = reduxForm({
    form: "LoginForm",
    validate,
})(Login);

const mapDispatchToProps = {
    postLogin,
};

export default connect(null, mapDispatchToProps)(Login);
