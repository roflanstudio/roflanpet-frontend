import React, { Component } from "react";
import "./Register.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Field, reduxForm, SubmissionError } from "redux-form";
import RenderField from "../../HelperComponents/RenderField/RenderField";
import { toast } from "react-toastify";
import AuthBtn from "./../../HelperComponents/AuthBtn/AuthBtn";
import { register } from "../../../redux/actions/authActions";

class Register extends Component {
    submitForm = (data) => {
        console.log(data);
        const { register, history } = this.props;
        return register(data).then((res) => {
            if (
                res.payload &&
                res.payload.status &&
                res.payload.status === 201
            ) {
                toast("You have registered successfully.");
                history.push("/auth/login");
            } else {
                console.log(res);
                throw new SubmissionError({
                    username:
                        res.error.response.data.username &&
                        res.error.response.data.username[0],
                    password:
                        res.error.response.data.password &&
                        res.error.response.data.password[0],
                    email:
                        res.error.response.data.email &&
                        res.error.response.data.email[0],
                    _error: res.error.response.data.non_field_errors[0],
                });
            }
        });
    };

    render() {
        const { handleSubmit, error } = this.props;
        return (
            <div className="auth-background">
                <div className="register">
                    <h1 className="register-title">Sign Up</h1>
                    <form onSubmit={handleSubmit(this.submitForm)}>
                        <div className="register-input">
                            <Field
                                name="first_name"
                                type="text"
                                placeholder="First name"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="last_name"
                                type="text"
                                placeholder="Last name"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="email"
                                type="email"
                                placeholder="Email"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="phone_number"
                                type="tel"
                                placeholder="Phone number"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="password"
                                type="password"
                                placeholder="Password"
                                component={RenderField}
                            />
                        </div>
                        <div className="register-input">
                            <Field
                                name="password2"
                                type="password"
                                placeholder="Repeat password"
                                component={RenderField}
                            />
                        </div>
                        {error && <span className="main-error">{error}</span>}
                        <div className="register-login">
                            <div className="register-login-text">
                                Do you have an account?
                            </div>
                            <Link
                                className="register-login-link"
                                to="/auth/login"
                            >
                                Sign in
                            </Link>
                        </div>
                        <AuthBtn />
                    </form>
                </div>
            </div>
        );
    }
}

const validate = (values) => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = "Required field";
    }
    if (!values.lastName) {
        errors.lastName = "Required field";
    }
    if (!values.email) {
        errors.email = "Required field";
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(values.email)
    ) {
        errors.email = "Invalid email address";
    }
    if (!values.phone) {
        errors.phone = "Required field";
    } else if (!/^\+?3?8?(0[5-9][0-9]\d{7})$/i.test(values.phone)) {
        errors.phone = "Phone number must be in format +380XXXXXXXXX";
    }
    if (!values.password) {
        errors.password = "Required field";
    } else if (values.password.length < 4) {
        errors.password = "Password must contain 4 or more symbols";
    }
    if (!values.password2) {
        errors.password2 = "Required field";
    } else if (values.password !== values.password2) {
        errors.password2 = "Passwords are not the same";
    }
    return errors;
};

Register = reduxForm({
    form: "RegisterForm",
    validate,
})(Register);

const mapDispatchToProps = {
    register,
};

export default connect(null, mapDispatchToProps)(Register);
