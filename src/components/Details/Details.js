import React, { useEffect, useRef } from 'react'

import './Details.scss'
import { connect } from 'react-redux'
import { getDetails } from './../../redux/actions/catalogActions'
import Moment from 'react-moment'
import FadeLoader from 'react-spinners/FadeLoader'
import { useState } from 'react'
import { getQrHits, getHits } from './../../redux/actions/profileActions'
import ReactToPrint from 'react-to-print'
import QRCode from 'react-qr-code'

class ComponentToPrint extends React.Component {
    render() {
        const { details } = this.props
        return (
            <div className='details'>
                <div className='head-details'>
                    {`${
                        details.status && details.status === 's'
                            ? 'Пропал(a)'
                            : 'Найден(a)'
                    } ${
                        details.pet &&
                        details.pet.species &&
                        details.pet.species.name
                            ? details.pet.species.name
                            : 'питомец'
                    }`}
                </div>
                <div className='city-details'>
                    {`Город: ${
                        details.city && details.city.name
                            ? details.city.name
                            : 'Неизвестен'
                    }`}
                </div>
                <div className='main-data-details'>
                    {details.pet &&
                        details.pet.breed &&
                        details.pet.breed.name && (
                            <div>
                                Порода:{' '}
                                {details.breed && details.breed.name
                                    ? details.breed.name
                                    : 'неизвестна'}
                            </div>
                        )}
                    {details.pet && details.pet.gender && (
                        <div>
                            Пол:{' '}
                            {details.pet.gender === 'm' ? 'мужской' : 'женский'}
                        </div>
                    )}
                    {details.pet && details.pet.color && (
                        <div>Цвет: {details.pet.color}</div>
                    )}
                </div>
                <div className='data-with-img'>
                    <div>
                        <p>
                            Статус:{' '}
                            <span
                                className={details.resolved ? 'green' : 'red'}>
                                {details.resolved ? 'Найден' : 'Не найден'}
                            </span>
                        </p>
                        <div>
                            дата находки:{' '}
                            <Moment format='DD MMMM YYYY'>
                                {details.created}
                            </Moment>
                        </div>
                    </div>
                    {details.pet &&
                        details.pet.petImage &&
                        details.pet.petImage.length > 0 && (
                            <img
                                src={`https://kyiv-smart-tourism.s3.amazonaws.com/media/${details.pet.petImage[0].pic}`}
                            />
                        )}
                </div>
                <QRCode value={window.location.href + '?qrcode=true'} />
            </div>
        )
    }
}

const Details = ({
    match: {
        params: { id },
    },
    query,
    getDetails,
    details,
    getHits,
    getQrHits,
}) => {
    const [loading, setLoading] = useState(true)
    useEffect(() => {
        Promise.all([getDetails(id), getHits(id)]).then(values => {
            setLoading(false)
        })
        console.log(query)
        query.qrcode === 'true' && getQrHits(id)
    }, [])
    let componentRef = useRef()
    return (
        <>
            <div style={{ display: 'none' }}>
                <ComponentToPrint
                    ref={el => (componentRef = el)}
                    details={details}
                />
            </div>
            {loading && (
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'center',
                        paddingTop: '400px',
                    }}>
                    <FadeLoader />
                </div>
            )}
            {!loading && (
                <div className='details'>
                    <div className='head-details'>
                        {`${
                            details.status && details.status === 's'
                                ? 'Пропал(a)'
                                : 'Найден(a)'
                        } ${
                            details.pet &&
                            details.pet.species &&
                            details.pet.species.name
                                ? details.pet.species.name
                                : 'питомец'
                        }`}
                    </div>
                    <div className='city-details'>
                        {`Город: ${
                            details.city && details.city.name
                                ? details.city.name
                                : 'Неизвестен'
                        }`}
                    </div>
                    <div className='main-data-details'>
                        {details.pet &&
                            details.pet.breed &&
                            details.pet.breed.name && (
                                <div>
                                    Порода:{' '}
                                    {details.breed && details.breed.name
                                        ? details.breed.name
                                        : 'неизвестна'}
                                </div>
                            )}
                        {details.pet && details.pet.gender && (
                            <div>
                                Пол:{' '}
                                {details.pet.gender === 'm'
                                    ? 'мужской'
                                    : 'женский'}
                            </div>
                        )}
                        {details.pet && details.pet.color && (
                            <div>Цвет: {details.pet.color}</div>
                        )}
                    </div>
                    <div className='details-btns'>
                        <a
                            href={`mailto: ${
                                details.creator && details.creator.email
                            }`}>
                            {details.status && details.status === 's'
                                ? 'Написать нашедшему'
                                : 'Написать владельцу'}
                        </a>
                        <ReactToPrint
                            trigger={() => (
                                <div>
                                    <svg
                                        width='49'
                                        height='49'
                                        viewBox='0 0 49 49'
                                        fill='none'
                                        xmlns='http://www.w3.org/2000/svg'>
                                        <path
                                            d='M40.1953 12.2596V1.53125H8.80469V12.2596C6.84366 12.3601 4.99597 13.2093 3.64265 14.632C2.28933 16.0548 1.53358 17.9426 1.53125 19.9062V38.2812H8.03906V35.2188H4.59375V19.9062C4.59512 18.6883 5.07954 17.5207 5.94074 16.6595C6.80194 15.7983 7.96958 15.3139 9.1875 15.3125H39.8125C41.0304 15.3139 42.1981 15.7983 43.0593 16.6595C43.9205 17.5207 44.4049 18.6883 44.4062 19.9062V35.2188H40.1953V38.2812H47.4688V19.9062C47.4664 17.9426 46.7107 16.0548 45.3574 14.632C44.004 13.2093 42.1563 12.3601 40.1953 12.2596ZM37.1328 12.25H11.8672V4.59375H37.1328V12.25Z'
                                            fill='white'
                                        />
                                        <path
                                            d='M37.8984 19.1406H40.9609V22.2031H37.8984V19.1406Z'
                                            fill='white'
                                        />
                                        <path
                                            d='M11.1016 25.2656H7.27344V28.3281H11.1016V47.4688H37.1328V28.3281H40.9609V25.2656H11.1016ZM34.0703 44.4062H14.1641V28.3281H34.0703V44.4062Z'
                                            fill='white'
                                        />
                                    </svg>
                                </div>
                            )}
                            content={() => componentRef}
                            parentContainer={{
                                '@media print': {
                                    display: 'block',
                                },
                            }}
                        />
                    </div>
                    <div className='data-with-img'>
                        <div>
                            <p>
                                Статус:{' '}
                                <span
                                    className={
                                        details.resolved ? 'green' : 'red'
                                    }>
                                    {details.resolved ? 'Найден' : 'Не найден'}
                                </span>
                            </p>
                            <div>
                                дата находки:{' '}
                                <Moment format='DD MMMM YYYY'>
                                    {details.created}
                                </Moment>
                            </div>
                        </div>
                        {details.pet &&
                            details.pet.petImage &&
                            details.pet.petImage.length > 0 && (
                                <img
                                    src={`https://kyiv-smart-tourism.s3.amazonaws.com/media/${details.pet.petImage[0].pic}`}
                                />
                            )}
                    </div>
                </div>
            )}
        </>
    )
}

const mapStateToProps = state => {
    return {
        query: state.router.location.query,
        details: state.catalog.details,
    }
}

const mapDispatchToProps = {
    getDetails,
    getQrHits,
    getHits,
}

export default connect(mapStateToProps, mapDispatchToProps)(Details)
