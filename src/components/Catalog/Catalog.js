import React, { Component } from 'react'
import './Catalog.scss'
import { connect } from 'react-redux'
import {
    getCity,
    getBreed,
    getAllPosts,
    getSpecies,
} from './../../redux/actions/catalogActions'
import PostCard from './../Profile/PostCard'
import FormControl from '@material-ui/core/FormControl'
import SelectComponent from './../HelperComponents/SelectComponent/SelectComponent'
import FadeLoader from 'react-spinners/FadeLoader'

class Catalog extends Component {
    state = {
        statusType: { label: 'Статус', value: '' },
        statusTypes: [
            { label: 'Ищу', value: 's' },
            { label: 'Нашел', value: 'f' },
        ],
        cityType: { label: 'Город', value: '' },
        genderType: { label: 'Пол', value: '' },
        genderTypes: [
            { label: 'Мужской', value: 'm' },
            { label: 'Женский', value: 'f' },
        ],
        sizeType: { label: 'Размер', value: '' },
        sizeTypes: [
            { label: 'Очень маленький', value: 't' },
            { label: 'Маленький', value: 's' },
            { label: 'Средний', value: 'm' },
            { label: 'Большой', value: 'l' },
            { label: 'Огромный', value: 'h' },
        ],
        animalType: { label: 'Вид Животного', value: '' },
        breedType: { label: 'Порода Животного', value: '' },
        loading: true,
    }
    componentDidMount() {
        const { getCity, getAllPosts, getSpecies } = this.props
        Promise.all([
            getCity(),
            getSpecies(),
            getAllPosts('', '', '', '', '', ''),
        ]).then(values => {
            this.setState({ loading: false })
        })
    }

    handleFilter = (type, value) => {
        const { getAllPosts } = this.props
        const {
            statusType,
            cityType,
            genderType,
            sizeType,
            animalType,
            breedType,
        } = this.state
        if (type === 'status') {
            getAllPosts(
                value.value,
                cityType.value,
                genderType.value,
                sizeType.value,
                animalType.value,
                breedType.value
            )
            this.setState({ statusType: value })
        } else if (type === 'city') {
            getAllPosts(
                statusType.value,
                value.value,
                genderType.value,
                sizeType.value,
                animalType.value,
                breedType.value
            )
            this.setState({ cityType: value })
        } else if (type === 'gender') {
            getAllPosts(
                statusType.value,
                cityType.value,
                value.value,
                sizeType.value,
                animalType.value,
                breedType.value
            )
            this.setState({ genderType: value })
        } else if (type === 'size') {
            getAllPosts(
                statusType.value,
                cityType.value,
                genderType.value,
                value.value,
                animalType.value,
                breedType.value
            )
            this.setState({ sizeType: value })
        } else if (type === 'animal') {
            getAllPosts(
                statusType.value,
                cityType.value,
                genderType.value,
                sizeType.value,
                value.value,
                breedType.value
            )
            this.setState({
                animalType: value,
                breedType: { label: 'Порода Животного', value: '' },
            })
            this.props.getBreed(value.value)
        } else if (type === 'breed') {
            getAllPosts(
                statusType.value,
                cityType.value,
                genderType.value,
                sizeType.value,
                animalType.value,
                value.value
            )
            this.setState({ breedType: value })
        }

        // else {
        //     if (value !== null) {
        //         getProjects(
        //             this.compileRequest(null, null, `tasks_count=${value}`)
        //         )
        //         this.setState({ filter: `tasks_count=${value}` })
        //     } else {
        //         this.setState({ filter: null })
        //         getProjects(this.compileRequest(null, null, ''))
        //     }
        // }
    }

    render() {
        const {
            statusType,
            statusTypes,
            cityType,
            genderType,
            genderTypes,
            sizeType,
            sizeTypes,
            animalType,
            breedType,
        } = this.state
        const { city, species, breed, allPosts } = this.props
        console.log(allPosts)
        return (
            <>
                {this.state.loading && (
                    <div
                        style={{
                            display: 'flex',
                            justifyContent: 'center',
                            paddingTop: '400px',
                        }}>
                        <FadeLoader />
                    </div>
                )}
                {!this.state.loading && (
                    <div className='catalog'>
                        <div className='content'>
                            <div className='filters'>
                                <div className='head'>Filters</div>
                                <FormControl className='select_wrapper_filter'>
                                    <SelectComponent
                                        isSearchable={false}
                                        options={statusTypes}
                                        value={statusType}
                                        change={e =>
                                            this.handleFilter('status', e)
                                        }
                                    />
                                </FormControl>
                                <FormControl className='select_wrapper_filter'>
                                    <SelectComponent
                                        isSearchable={false}
                                        options={city.map(el => ({
                                            label: el.name,
                                            value: el.id,
                                        }))}
                                        value={cityType}
                                        change={e =>
                                            this.handleFilter('city', e)
                                        }
                                    />
                                </FormControl>
                                <FormControl className='select_wrapper_filter'>
                                    <SelectComponent
                                        isSearchable={false}
                                        options={genderTypes}
                                        value={genderType}
                                        change={e =>
                                            this.handleFilter('gender', e)
                                        }
                                    />
                                </FormControl>
                                <FormControl className='select_wrapper_filter'>
                                    <SelectComponent
                                        isSearchable={false}
                                        options={sizeTypes}
                                        value={sizeType}
                                        change={e =>
                                            this.handleFilter('size', e)
                                        }
                                    />
                                </FormControl>
                                <FormControl className='select_wrapper_filter'>
                                    <SelectComponent
                                        isSearchable={false}
                                        options={species.map(el => ({
                                            label: el.name,
                                            value: el.id,
                                        }))}
                                        value={animalType}
                                        change={e =>
                                            this.handleFilter('animal', e)
                                        }
                                    />
                                </FormControl>
                                <FormControl className='select_wrapper_filter'>
                                    <SelectComponent
                                        isSearchable={false}
                                        options={breed.map(el => ({
                                            label: el.name,
                                            value: el.id,
                                        }))}
                                        value={breedType}
                                        change={e =>
                                            this.handleFilter('breed', e)
                                        }
                                    />
                                </FormControl>
                                <div
                                    className='reset'
                                    onClick={() => {
                                        this.setState({
                                            statusType: {
                                                label: 'Статус',
                                                value: '',
                                            },
                                            cityType: {
                                                label: 'Город',
                                                value: '',
                                            },
                                            genderType: {
                                                label: 'Пол',
                                                value: '',
                                            },
                                            sizeType: {
                                                label: 'Размер',
                                                value: '',
                                            },
                                            animalType: {
                                                label: 'Вид Животного',
                                                value: '',
                                            },
                                            breedType: {
                                                label: 'Порода Животного',
                                                value: '',
                                            },
                                        })
                                        this.props.getAllPosts(
                                            '',
                                            '',
                                            '',
                                            '',
                                            '',
                                            ''
                                        )
                                    }}>
                                    Сбросить фильтры
                                </div>
                            </div>
                            <div className='list'>
                                {allPosts.map((el, idx) => {
                                    return (
                                        <PostCard
                                            id={el.id}
                                            key={el.id}
                                            pic={
                                                el.pet &&
                                                el.pet.petImage.length > 0
                                                    ? `https://kyiv-smart-tourism.s3.amazonaws.com/media/${
                                                          el.pet.petImage[0] &&
                                                          el.pet.petImage[0].pic
                                                      }`
                                                    : null
                                            }
                                            color={el.pet && el.pet.color}
                                            gender={el.pet && el.pet.gender}
                                            breed={
                                                el.pet.breed &&
                                                el.pet.breed.name
                                            }
                                            name={
                                                el.pet &&
                                                el.pet.name &&
                                                el.pet.name.substring(0, 7)
                                            }
                                            type={
                                                el.pet &&
                                                el.pet.species &&
                                                el.pet.species.name
                                            }
                                            size={el.pet && el.pet.size}
                                            status={
                                                el.status === 's'
                                                    ? 'Ищу'
                                                    : 'Нашел'
                                            }
                                            city={el.city && el.city.name}
                                        />
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        city: state.catalog.city,
        species: state.catalog.species,
        breed: state.catalog.breed,
        allPosts: state.catalog.allPosts,
    }
}

const mapDispatchToProps = { getCity, getBreed, getAllPosts, getSpecies }

export default connect(mapStateToProps, mapDispatchToProps)(Catalog)
