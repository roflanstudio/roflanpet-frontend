import React, { useEffect, useState } from 'react'
import './Header.scss'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { useLocation } from 'react-router-dom'
import CreateLeafletForm from '../../Main/CreateLeafletForm/CreateLeafletForm'

const Header = ({ push }) => {
    const [auth, setAuth] = useState(false)
    const [openModal, setOpenModal] = useState(false)
    const [modalType, setModalType] = useState(null)
    useEffect(() => {
        localStorage.getItem('token') && setAuth(true)
    }, [])
    const location = useLocation()
    return (
        <header>
            <Link to='/main/home' className='logo'>
                Petify
            </Link>
            {location.pathname === '/main/home' ? (
                <a className='bage' href='#badge'>
                    <div>
                        <span className='bage-first'>Жетон</span>
                        <span className='bage-second'>Petify</span>
                    </div>
                    <svg
                        width='16'
                        height='22'
                        viewBox='0 0 16 22'
                        fill='none'
                        xmlns='http://www.w3.org/2000/svg'>
                        <path
                            d='M7.29289 21.7071C7.68342 22.0976 8.31658 22.0976 8.70711 21.7071L15.0711 15.3431C15.4616 14.9526 15.4616 14.3195 15.0711 13.9289C14.6805 13.5384 14.0474 13.5384 13.6569 13.9289L8 19.5858L2.34314 13.9289C1.95262 13.5384 1.31946 13.5384 0.928931 13.9289C0.538407 14.3195 0.538407 14.9526 0.928931 15.3431L7.29289 21.7071ZM7 -6.24448e-08L7 21L9 21L9 6.24448e-08L7 -6.24448e-08Z'
                            fill='#B87C84'
                        />
                    </svg>
                </a>
            ) : (
                <div className='post-btns'>
                    <div
                        className='lost-btn'
                        onClick={() => {
                            setModalType('s')
                            setOpenModal(true)
                        }}>
                        Я Потерял Питомца
                    </div>
                    <div
                        className='find-btn'
                        onClick={() => {
                            setModalType('f')
                            setOpenModal(true)
                        }}>
                        Я Нашел Питомца
                    </div>
                </div>
            )}

            {location.pathname === '/main/profile/' ? (
                <div
                    className='login-redirect'
                    onClick={() => {
                        localStorage.removeItem('token')
                        push('/auth/login')
                    }}>
                    Logout
                </div>
            ) : auth ? (
                <Link to={'/main/profile/'} className='login-redirect'>
                    Your profile
                </Link>
            ) : (
                <Link to={'/auth/login/'} className='login-redirect'>
                    Sign In
                </Link>
            )}
            {openModal && (
                <CreateLeafletForm
                    isOpen={openModal}
                    type={modalType}
                    close={() => setOpenModal(false)}
                />
            )}
        </header>
    )
}

const mapDispatchToProps = {
    push,
}

export default connect(null, mapDispatchToProps)(Header)
