import React from "react";
import TextField from "@material-ui/core/TextField";

import "./RenderField.scss";

const RenderField = ({
    input,
    type,
    className,
    placeholder,
    meta: { touched, error, warning },
    color,
    multiline = false
}) => (
    <div>
        <TextField
            {...input}
            className={className}
            label={placeholder}
            type={type}
            variant="outlined"
            color={color}
            multiline={multiline}
        />
        {touched &&
            ((error && <span className="input-error">{error}</span>) ||
                (warning && <span>{warning}</span>))}
    </div>
);

export default RenderField;