import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { createBrowserHistory } from "history";
import { routerMiddleware, ConnectedRouter } from "connected-react-router";
import { multiClientMiddleware } from "redux-axios-middleware";
import { composeWithDevTools } from "redux-devtools-extension";

import api from "./redux/actions/api";
import routes from "./routes/routes";
import rootReducer from "./redux/reducers";

const axiosMiddlewareOptions = {
  interceptors: {
      request: [
          (action, config) => {
              if (localStorage.token || localStorage.token_res) {
                  let token = localStorage.token ? localStorage.token : localStorage.token_res;
                  config.headers["Authorization"] = "Token " + token;
              }
              return config;
          }
      ],
      response: [
          function ({ getState, dispatch, getSourceAction }, req) {
              if (req.status === 401) {
                  localStorage.clear();
              }
              return req;
          }
      ]
  }
};
const history = createBrowserHistory();
const appRouterMiddleware = routerMiddleware(history);
const createStoreWithMiddleware = applyMiddleware(
    multiClientMiddleware(api, axiosMiddlewareOptions),
    appRouterMiddleware,
    thunk
)(createStore);
const store = createStoreWithMiddleware(rootReducer(history), {}, composeWithDevTools());

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history} children={routes} />
    </Provider>,
    document.getElementById("root")
);
